import React, { Component, Fragment } from "react";
import { Header, Footer } from "./components/Layouts";
import { Banner, ContentList } from "./components";
import "./style.css";

export default class App extends Component {
  render() {
    return (
      <Fragment>
        <Header />
        <Banner />
        <ContentList />
        <Footer />
      </Fragment>
    );
  }
}
