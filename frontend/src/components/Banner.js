import React from "react";
import LandingBG from "../../public/img/landing-page.png";

const Banner = () => {
  return (
    <section
      style={{ backgroundImage: `url(${LandingBG})` }}
      className="jumbotron text-center"
    >
      <h1>เป็นมากกว่าแค่งาน</h1>
      <p>สมัครงาน กับ บริษัทชั้นนํา และ Startup ทั่ว Asia ได้ที่นี่</p>
      <p>
        <a className="btn btn-getlinks btn-lg" href="#" role="button">
          รู้จักกับเรามากขึ้น
        </a>
      </p>
    </section>
  );
};

export default Banner;
