import React, { Component, Fragment } from "react";

export default class Header extends Component {
  render() {
    return (
      <Fragment>
        <header className="container text-center">
          <img
            className="img-logo"
            src="https://getlinks.co/images/v2/getlinks_full_logo.png"
          />
          <div className="text-center">
            <ul className="getlink-nav">
              <li className="active">
                <a href="#">Home</a>
              </li>
              <li>
                <a href="#">About</a>
              </li>
              <li>
                <a href="#">Features</a>
              </li>
              <li>
                <a href="#">Contract Us</a>
              </li>
            </ul>
          </div>
        </header>
      </Fragment>
    );
  }
}
