import React, { Component } from "react";

export default class Footer extends Component {
  render() {
    return (
      <footer className="p-2">
        <div className="container">
          <div className="row">
            <div className="col-md-3">
              <div className="caption-footer">
                <h4>เกี่ยวกับเรา</h4>
                <ul>
                  <li>เรื่องเกี่ยวกับเรา </li>
                  <li>ร่วมงานกับเรา </li>
                  <li>บทความ </li>
                  <li>เงื่อนไขการให้บริการ </li>
                  <li>นโยบายความเป็นส่วนตัว </li>
                  <li>ติดต่อเรา </li>
                </ul>
              </div>
            </div>
            <div className="col-md-3">
              <div className="caption-footer">
                <h4>สำหรับคนหางาน</h4>
                <ul>
                  <li>แนะนำให้เพื่อน</li>
                  <li>คำถามที่พบบ่อย</li>
                </ul>
              </div>
            </div>
            <div className="col-md-3">
              <div className="caption-footer">
                <h4>สำหรับบริษัท</h4>
                <ul>
                  <li>การทำสัญญา</li>
                  <li>คำถามที่พบบ่อย</li>
                </ul>
              </div>
            </div>
            <div className="col-md-3">
              <div className="caption-footer">
                <img
                  className="img-logo"
                  src="https://getlinks.co/images/v2/getlinks_full_logo.png"
                />
                <p>GetLinks แหล่งรวบรวมบุคลากรเทคโนโลยีชั้นนำแห่งเอเชีย</p>
                <a href="#" class="fa fa-facebook" />
                <a href="#" class="fa fa-twitter" />
                <a href="#" class="fa fa-linkedin" />
                <a href="#" class="fa fa-youtube" />
              </div>
            </div>
          </div>
        </div>
      </footer>
    );
  }
}
