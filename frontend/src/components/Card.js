import React, { Component } from "react";

const Card = props => {
  const { title, img } = props;
  return (
    <div className="col-xs-12 col-sm-4 col-md-3 wrapper-card">
      <div className={"card"}>
        <img className={"img-cover"} src={img} />
        <div className="caption">
          <h4>{title}</h4>
        </div>
      </div>
    </div>
  );
};

export default Card;
