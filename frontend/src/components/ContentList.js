import React, { Component } from "react";
import Card from "./Card";
import MockData from "../mock.json";

export default class ContentList extends Component {
  render() {
    return (
      <section className="container content-list">
        {MockData.map(({ title, _id, img }, index) => (
          <Card key={_id} title={title} img={img} />
        ))}
      </section>
    );
  }
}
