require("dotenv").config();
const { Client } = require("pg");
//connection
const db = new Client({
  user: process.env.DB_USER,
  host: process.env.DB_HOST || "localhost",
  database: process.env.DB_NAME,
  password: "",
  port: process.env.DB_PORT || 5432
});

db.connect();
module.exports = db;
