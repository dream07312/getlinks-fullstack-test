const MembersController = require("./controller");

module.exports = function setup(server) {
  server.post("/members/register", MembersController.register);
  server.post("/members/login", MembersController.login);
  server.get("/members/:id", MembersController.getId);
};
