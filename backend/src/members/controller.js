const Members = require("./model");

const MembersController = {
  register: async function(req, res, next) {
    if (!req.body || !req.body.email || !req.body.password) {
      return res.json(400, {
        status: false,
        error: false,
        msg: "Invalid email and password."
      });
    }
    const { error, status, msg, data = {} } = await Members.create(req.body);
    if (!error) {
      return res.json(200, { status, msg, data });
    } else {
      return res.json(501, { error, status, msg });
    }
  },
  login: async function(req, res, next) {
    if (!req.body || !req.body.email || !req.body.password) {
      return res.json(400, {
        status: false,
        error: false,
        msg: "Invalid email and password."
      });
    }
    const { error, status, msg, data = {} } = await Members.session(req.body);
    if (!error) {
      return res.json(200, { status, msg, data });
    } else {
      return res.json(501, { error, status, msg });
    }
  },
  getId: async function(req, res, next) {
    Members.verifyToken(req, res, async () => {
      const response = await Members.get(req.params.id);
      res.json(200, { status: true, data: response });
    });
  }
};

module.exports = MembersController;
