require("dotenv").config();
const db = require("../db");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const saltRounds = 13;

const MembersModel = {
  create: async function(data) {
    try {
      const passwordEncrypt = await bcrypt.hash(data.password, saltRounds);
      const query = {
        text:
          "INSERT INTO members(email,password,created_at,updated_at) VALUES($1, $2 , $3, $4)",
        values: [data.email, passwordEncrypt, new Date(), new Date()]
      };
      const response = await db.query(query);
      return {
        error: null,
        status: true,
        msg: "Regsiter Successfully",
        data: { email: data.email }
      };
    } catch (error) {
      return { error: error, status: false, msg: "Fail to record to db" };
    }
  },
  verifyToken: async function(req, res, next) {
    const bearerHeader = req.headers["authorization"];
    if (typeof bearerHeader !== "undefined") {
      const bearer = bearerHeader.split(" ");
      const bearerToken = bearer[1];
      req.token = bearerToken;
      jwt.verify(req.token, process.env.SECRET_KEY, (err, authData) => {
        if (err) return res.json(403, { error: "Invalid Token" });
        next();
      });
    } else {
      return res.json(403, { error: "Invalid Token" });
    }
  },
  genToken: async function(data) {
    return new Promise((resolve, reject) => {
      jwt.sign(
        { user: data.email },
        "getlinks_key",
        {
          expiresIn: "1hr"
        },
        (err, token) => {
          if (err) reject({ error: err });
          resolve({ error: null, token: token });
        }
      );
    });
  },
  session: async function(data) {
    try {
      const query = {
        text: "SELECT * FROM members WHERE email = $1",
        values: [data.email]
      };
      const response = await db.query(query);
      const passwordEncrypt = response.rows[0].password;
      const validPassword = await bcrypt.compare(
        data.password,
        passwordEncrypt
      );
      const queryUpdate = {
        text: "UPDATE members SET updated_at = $2 WHERE email = $1",
        values: [data.email, new Date()]
      };
      const updatedAt = await db.query(queryUpdate);
      if (!validPassword) throw "Invalid your password";
      const { token } = await this.genToken(data);
      return {
        error: null,
        status: true,
        msg: "Login Successfully",
        data: { email: data.email, token: token }
      };
    } catch (error) {
      return { error: error, status: false, msg: "Fail to Login" };
    }
  },
  get: async function(data) {
    try {
      const query = {
        text: "SELECT * FROM members WHERE member_id = $1",
        values: [data]
      };
      const response = await db.query(query);
      return {
        email: response.rows[0].email,
        memberId: response.rows[0].member_id
      };
    } catch (error) {
      return { msg: "Not Found member" };
    }
  }
};

module.exports = MembersModel;
