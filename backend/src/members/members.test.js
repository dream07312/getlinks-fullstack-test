const expect = require("expect");
const request = require("supertest");
const server = require("../../server");
var chai = require("chai").expect;

const mockData = { email: "tester@test.com", password: "1234" };

describe("POST /members/register", () => {
  it("should create a new members", done => {
    request(server)
      .post("/members/register")
      .send(mockData)
      .expect(200)
      .expect(res => {
        expect(res.body.status).toBe(true);
        expect(res.body.data.email).toBe(mockData.email);
      })
      .end(done);
  });

  it("should not create members with invalid body data", done => {
    request(server)
      .post("/members/register")
      .send({})
      .expect(400)
      .expect(res => {
        expect(res.body.status).toBe(false);
        chai(res.body).to.have.own.property("error");
      })
      .end(done);
  });

  it("should not invalid because email is unique key", done => {
    request(server)
      .post("/members/register")
      .send(mockData)
      .expect(501)
      .expect(res => {
        expect(res.body.status).toBe(false);
        chai(res.body).to.have.own.property("error");
      })
      .end(done);
  });
});

describe("POST /members/login", () => {
  it("should login and receive token ", done => {
    request(server)
      .post("/members/login")
      .send(mockData)
      .expect(200)
      .expect(res => {
        expect(res.body.status).toBe(true);
        expect(res.body.data.email).toBe(mockData.email);
        chai(res.body.data).to.have.own.property("token");
      })
      .end(done);
  });

  it("should not with invalid body data ", done => {
    request(server)
      .post("/members/login")
      .send({})
      .expect(400)
      .expect(res => {
        expect(res.body.status).toBe(false);
        chai(res.body).to.have.own.property("error");
      })
      .end(done);
  });
});
