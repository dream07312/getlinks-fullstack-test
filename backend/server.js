require("dotenv").config();
const restify = require("restify");
const members = require("./src/members/controller");
const fs = require("fs");
const server = restify.createServer({
  name: process.env.SERVER_NAME,
  version: "1.0.0"
});

server.use(restify.plugins.acceptParser(server.acceptable));
server.use(restify.plugins.queryParser());
server.use(restify.plugins.bodyParser());

function setupRouteInit(server) {
  const APP_DIR = `${__dirname}/src`;
  const domains = fs
    .readdirSync(APP_DIR)
    .filter(file => fs.statSync(`${APP_DIR}/${file}`).isDirectory());

  domains.forEach(domain => {
    const routes = require(`${APP_DIR}/${domain}/routes.js`);
    routes(server);
  });
}

setupRouteInit(server);

server.listen(process.env.PORT || 3000, function() {
  console.log("%s listening at %s", server.name, server.url);
});

module.exports = server;
