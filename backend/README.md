# Getlinks Test Backend

File DB backup at backup-db diretory

## Start project

```sh
npm install
npm run test
npm run start
```

| Route             | Method | Desc                                     |
| ----------------- | ------ | ---------------------------------------- |
| /members/regsiter | POST   | Register member                          |
| /members/login    | POST   | Login and receive token for get id       |
| /members/:id      | GET    | Get member by id by your must have token |
